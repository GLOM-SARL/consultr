import { Navigate } from 'react-router';
import NxWelcome from '../app/nx-welcome';

export const routes = [
    { path: '/', element: <NxWelcome title="consultr"/> },
    // { path: '/', element: <Typography>Hello world</Typography> },
    { path: '*', element: <Navigate to="/" /> },
]