import { createTheme } from "@mui/material/styles";
import React from "react";

declare module "@mui/material/styles" {
  interface Theme {
    common: {
      line: React.CSSProperties["color"];
      inputBackground: React.CSSProperties["color"];
      background: React.CSSProperties["color"];
      offWhite: React.CSSProperties["color"];
      placeholder: React.CSSProperties["color"];
      label: React.CSSProperties["color"];
      body: React.CSSProperties["color"];
      dialogBackground: React.CSSProperties["color"]
    };
  }
  interface ThemeOptions {
    common: {
      line: React.CSSProperties["color"];
      inputBackground: React.CSSProperties["color"];
      background: React.CSSProperties["color"];
      offWhite: React.CSSProperties["color"];
      placeholder: React.CSSProperties["color"];
      label: React.CSSProperties["color"];
      body: React.CSSProperties["color"];
      dialogBackground: React.CSSProperties["color"]
    };
  }
  interface TypographyVariants {
    h1: React.CSSProperties;
    h2: React.CSSProperties;
    h3: React.CSSProperties;
    h4: React.CSSProperties;
    h5: React.CSSProperties;
    h6: React.CSSProperties;
    body1: React.CSSProperties;
    body2: React.CSSProperties;
    body3: React.CSSProperties;
  }

  // allow configuration using `createTheme`
  interface TypographyVariantsOptions {
    h1: React.CSSProperties;
    h2: React.CSSProperties;
    h3: React.CSSProperties;
    h4: React.CSSProperties;
    h5: React.CSSProperties;
    h6: React.CSSProperties;
    body1: React.CSSProperties;
    body2: React.CSSProperties;
    body3: React.CSSProperties;
  }
}

// Update the Typography's variant prop options
declare module "@mui/material/Typography" {
  interface TypographyPropsVariantOverrides {
    h1: true;
    h2: true;
    h3: true;
    h4: true;
    h5: true;
    h6: true;
    body1: true;
    body2: true;
    body3: true;
  }
}

const theme = createTheme({
  palette: {
    primary: {
      main: "#6BBE16",
    },
    secondary: {
      main: "#272F32",
    },
    error: {
      main: "#DD0303",
    },
    success: {
      main: "#00BA88",
    },
  },
  common: {
    line: "#FFFFFF2E",
    inputBackground: "#F4F5F7",
    background:
      "radial-gradient(100% 100% at 50% 0%, #343537 0%, #000000 100%)",
    offWhite: "rgba(255,255,255,0.6)",
    placeholder: "#A0A3BD",
    label: "#FDFDFD",
    body: "#0F325A",
    dialogBackground: "#2E393D"
  },
  typography: {
    fontFamily: [
      "Poppins",
      "Roboto",
      "serif",
    ].join(","),
    h1: {
      fontSize: "3rem",
      fontWeight: 300,
      fontStyle: "normal",
    },
    h2: {
      fontSize: "2rem",
      fontWeight: 300,
      fontStyle: "normal",
    },
    h3: {
      fontSize: "1.5rem",
      fontWeight: 300,
      fontStyle: "normal",
    },
    h4: {
      fontSize: "1.3rem",
      fontWeight: 300,
      fontStyle: "normal",
    },
    h5: {
      fontSize: "1.1rem",
      fontWeight: 300,
      fontStyle: "normal",
    },
    h6: {
      fontSize: "1rem",
      fontWeight: 300,
      fontStyle: "normal",
    },
    body1: {
      fontSize: "0.9rem",
      fontWeight: 300,
      fontStyle: "normal",
    },
    body2: {
      fontSize: "0.8rem",
      fontWeight: 300,
      fontStyle: "normal",
    },
    body3: {
      fontSize: "0.6rem",
      fontWeight: 300,
      fontStyle: "normal",
    },
  },
});

export default theme;
